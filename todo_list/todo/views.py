from todo.models import TodoTab
from todo.serializers import TodoListSerializer
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from django.http import HttpResponse,JsonResponse,HttpResponseRedirect,Http404
from django.template import loader,Context,Template
from django.shortcuts import render
from .forms import post_form
import requests


def get_response_by_url(request):
    url="http://127.0.0.1:8000/api/v1/todo/"
    response=requests.get(url)
    context=response.json()
    form1=post_form
    return render(request,'todo/todo_home.html',context={'context':context, 'form':form1})    

def nav(request):
    template=loader.get_template('todo/index.html')
    context={}
    return HttpResponse(template.render(context,request))

def fetch(request):
    template=loader.get_template('todo/fetch.html')
    context={}
    return HttpResponse(template.render(context,request))



class TodoTabList(APIView):
    def get(self, request, format=None):
        todos = TodoTab.objects.all()
        serializer = TodoListSerializer(todos, many=True)
        return JsonResponse(serializer.data,safe=False)

    def post(self, request, format=None):
        form=post_form(request.POST)
        serializer = TodoListSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return  Response(serializer.data,status=status.HTTP_201_CREATED)
            # return HttpResponseRedirect(redirect_to='http://127.0.0.1:8000/api/v1/fetch/')
            # return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        

class TodoTabDetail(APIView):
   
    def get_object(self, pk):
        try:
            return TodoTab.objects.get(pk=pk)
        except TodoTab.DoesNotExist:
            raise Http404

    def get(self, request, pk, format=None):
        snippet = self.get_object(pk)
        serializer = TodoListSerializer(snippet)
        return Response(serializer.data)


        
    def put(self, request, pk, format=None):
        todo = self.get_object(pk)
        serializer = TodoListSerializer(todo, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data,status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk, format=None):
        data = self.get_object(pk)
        data.delete()
        data={
            'id':pk
        }
        return JsonResponse(data,status=status.HTTP_200_OK)
        



