from django.urls import path,re_path
from . import views
from django.conf.urls import url

urlpatterns = [
    path('todos/', views.TodoTabList.as_view()),
    re_path(r'^todo/(?P<pk>[0-9]+)/$', views.TodoTabDetail.as_view()),
    # path('index/', views.nav,name='nav'),
    # path('fetch/', views.get_response_by_url,name='get_response_by_url'),
]

 