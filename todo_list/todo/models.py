
from django.db import models

class TodoTab(models.Model):

    task = models.CharField(max_length=500,null=False)
    status = models.IntegerField(default=1)
    task_creation_date = models.DateField(auto_now_add=True)
    task_update_date = models.DateField(auto_now=True)
    
    def __str__(self):
        return self.task
