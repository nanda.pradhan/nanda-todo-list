from rest_framework import serializers
from . import models


class TodoListSerializer(serializers.ModelSerializer):

    class Meta:
        model = models.TodoTab
        fields ='__all__'